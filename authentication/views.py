from django.contrib.auth import logout
from django.shortcuts import render, redirect
from django.http import HttpResponse


def login_view(request):
    """
    Method responsible for loading the authentication page.
    """

    if not request.user.is_authenticated:
        return render(request, 'login.html')
    else:
        return redirect('/')


def logout_view(request):
    """
    Method responsible for de-authenticating the user.
    """

    if request.user.is_authenticated:
        logout(request)
        return redirect('/')
    else:
        return redirect('/')
